import LoginForm from "./LoginForm";
import "bootstrap/dist/js/bootstrap.bundle";
import useToken from "@galvanize-inc/jwtdown-for-react";

export const Main = () => {
  const { token } = useToken();
  return (
    <div>
      {!token && <LoginForm />}
    </div>
  );
};
